﻿const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack-build/webpack-common.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');



config = {
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
        ]
    },
    plugins: [
    
        new CopyPlugin({
            patterns: [
                {
                    from: "./src/assets/**/*",
                    to: path.resolve(__dirname, "wwwroot/assets/[name].[ext]")
                },
            ],

        }),
       
    ]
};

let configOutput = merge(config, commonConfig);


module.exports = configOutput;

