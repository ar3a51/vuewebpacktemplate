﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VueWebpackTemplate.webpack
{
    public static class WebpackLoader
    {
        private static Dictionary<string, List<string>> tags = new Dictionary<string, List<string>>();

        public static void Init()
        {
            var fs = File.OpenRead(@"webpack/stats.json");
            var streamReader = new StreamReader(fs);

            var reader = new JsonTextReader(streamReader);

            var obj = JObject.Load(reader);

            var fileObjects = obj["assetsByChunkName"];

            foreach(var file in fileObjects)
            {
                JProperty prop = (JProperty)file;

                var tokenList = prop.Value.ToList();
                var fileList = new List<string>();
                foreach(var token in tokenList)
                {
                    fileList.Add(token.ToString());
                }

                tags.Add(prop.Name, fileList);
            }           
        }
        public static string[] GetJS(string name)
        {
            var jses = new List<string>();

            var keys = tags.Where(t => t.Key.StartsWith(name));
            foreach (var entry in keys)
            {
                var css = entry.Value.Where(k => k.Contains(".js")).ToList();
                jses.AddRange(css);
            }

            return jses.ToArray();
        }

        public static string [] GetCSS(string name)
        {
            var csses = new List<string>();

            var entries = tags.Where(t => t.Key.StartsWith(name));
            foreach(var entry in entries)
            {
                var css = entry.Value.Where(k => k.Contains(".css")).ToList();
                csses.AddRange(css);
            }

            return csses.ToArray();
        }
    }
}
