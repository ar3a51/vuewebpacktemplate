﻿import axios from 'axios';
import 'es6-promise/auto';


export class HttpService {

    constructor() {
        this.httpService = axios;
    }

    post(url) {
        return this.httpService.post(url).then(result => result.data);
    }

    get(url) {
        return this.httpService.get(url).then(result => result.data);
    }

    delete(url) {
        return this.httpService.delete(url).then(result => result.data);
    }
}