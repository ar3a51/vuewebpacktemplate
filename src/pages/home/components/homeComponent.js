﻿import Vue from 'vue';
import { HomeService } from '../HomeService';


export const homeBlock = Vue.component('homeBlock', {
    data() {
        return {
            message: ""
        }
    },
    template: `
        <h1>This is home block</h1>
    `,
    mounted() {
        let svc = new HomeService();

        svc.requestData().then(rs => console.log('homeBlock: ' + rs));
    }
});