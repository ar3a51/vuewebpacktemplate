﻿import './index.scss';
import { HttpService } from '../../shared/services/httpService';
new Vue({
    el: "#app",
    data: {
        message: "hello"
    },
    mounted() {
        let svc = new HttpService();
        svc.get("/assets/test.json").then(data => console.log(data));
    }

});