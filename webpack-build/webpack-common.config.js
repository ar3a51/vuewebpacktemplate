﻿const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { StatsWriterPlugin } = require('webpack-stats-plugin');
const { entryPoints } = require('./entry-points');

module.exports = {
    target: ['web','es5'],
    entry: {
        // main: glob.sync("./src/pages/**/*.js"),
        ...entryPoints
       
    },
    output: {
        filename: '[name].[contenthash].js',
        path: `${appRoot}/wwwroot/dist`,
        chunkFilename:'scripts/[name].chunk.js'
       
       
    },
    externals: {
        vue: 'Vue',
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            inject: false,
            template: `${appRoot}/src/layout.html`,
            filename: `${appRoot}/Views/Shared/_Layout.cshtml`,
            publicPath: '~/dist/',
            scriptLoading: 'blocking',
            chunks: ['vendor', 'shared', 'maincss']

        }),
        new StatsWriterPlugin({
            filename: "../../webpack/stats.json"
        }),
       
       
    ],
    module: {
        rules: [
            {
                test: /\.m?js$/, exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    }
                }
            },
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name:'vendor'
        },
    }
};

