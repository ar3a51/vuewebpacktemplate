﻿const glob = require('glob');
const path = require('path');
const { appRoot } = require('../globalRoot');
global.appRoot = appRoot;

const entryPoints = {
    shared: glob.sync(`${appRoot}/src/shared/**/*.js`),
    home: { import: `${appRoot}/src/pages/home/index.js`, dependOn: 'shared' },
    about: { import: `${appRoot}/src/pages/about/index.js`, dependOn: 'shared' },
    maincss: `${appRoot}/src/shared/css/shared.scss`
};

module.exports = {
    entryPoints
};